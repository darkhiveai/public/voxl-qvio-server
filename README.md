# voxl-qvio-server

MPA service for Qualcomm MV SDK VIO (MVVISLAM)

Due to MV SDK being 32-bit only, this is the only 32-bit package for QRB5165. The APQ8096 package builds in voxl-emulator, the QRB5165 package builds in qrb5165-emulator using a 32-bit cross compiler installed in the emulator.


## Build dependencies

* libmodal_pipe
* libmodal_json
* voxl-mpa-tools
* voxl-mavlink
* mv (included in emulator docker images)

This README covers building this package.


## Build Instructions

1) prerequisite: voxl-emulator docker image >= v1.7 for APQ8096, qrb5165-emulator (>=1.3) for qrb5165 builds.

Follow the instructions here to set up the docker environment:

https://gitlab.com/voxl-public/voxl-docker


## Build Instructions

1) Launch the voxl-emulator or qrb5165-emulator docker.

```bash
~/git/voxl-qvio-server$ voxl-docker -i voxl-emulator
OR
~/git/voxl-qvio-server$ voxl-docker -i qrb5165-emulator
```

2) Install dependencies inside the docker. You must specify both the hardware platform and binary repo section to pull from. CI will use the `dev` binary repo for `dev` branch jobs, otherwise it will select the correct target SDK-release based on tags. When building yourself, you must decide what your intended target is, usually `dev` or `staging`

```bash
voxl-emulator:~$ ./install_build_deps.sh apq8096 staging
OR
qrb5165-emulator:~$ ./install_build_deps.sh qrb5165 staging
```


3) Build scripts should take the hardware platform as an argument: `qrb5165` or `apq8096`. CI will pass these arguments to the build script based on the job target. It's recommended your build script also allow a `native` option that uses the native GCC instead of a cross-compiler. This is handy for local building and testing of hard-ware independent code.

The build script in this template will build for 64-bit using different cross compilers for each hardware platform. See libmodal-pipe for an exmaple of building for both 64 and 32 bit.

```bash
voxl-emulator:~$ ./build.sh apq8096
OR
qrb5165-emulator:~$ ./build.sh qrb5165
```


4) Make an ipk or deb package while still inside the docker.

```bash
qrb5165-emulator:~$ ./make_package.sh deb
OR
voxl-emulator:~$ ./make_package.sh ipk
```

This will make a new package file in your working directory. The name and version number came from the package control file. If you are updating the package version, edit it there.

Optionally add the --timestamp argument to append the current data and time to the package version number in the debian package. This is done automatically by the CI package builder for development and nightly builds, however you can use it yourself if you like.


## Deploy to VOXL

You can now push the ipk or deb package to VOXL and install with dpkg/opkg however you like. To do this over ADB, you may use the included helper script: deploy_to_voxl.sh. Do this outside of docker as your docker image probably doesn't have usb permissions for ADB.

The deploy_to_voxl.sh script will query VOXL over adb to see if it has dpkg installed. If it does then then .deb package will be pushed an installed. Otherwise the .ipk package will be installed with opkg.

```bash
(outside of docker)
voxl-qvio-server$ ./deploy_to_voxl.sh
```

This deploy script can also push over a network given sshpass is installed and the VOXL uses the default root password.


```bash
(outside of docker)
voxl-qvio-server$ ./deploy_to_voxl.sh ssh 192.168.1.123
```
