#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdint.h>
#include <unistd.h>

#define PX4_LISTENER_BUFSIZE 128

static pthread_t slpi_gps_thread;

static int _starts_with(const char *str, const char* start)
{
	return strncmp(start, str, strlen(start)) == 0;
}

typedef struct gps_v_data_t{
	int64_t ts_monotonic_us;
	int64_t time_utc_us;
	float vn;
	float ve;
	float vd;
	float v_variance;
	int fix_type;
} gps_v_data_t;

static void _reset_gps_v_data(gps_v_data_t* d)
{
	d->ts_monotonic_us = -1;
	d->time_utc_us = -1;
	d->vn = NAN;
	d->ve = NAN;
	d->vd = NAN;
	d->v_variance = NAN;
	d->fix_type = -1;
}

static int _is_gps_v_data_complete(gps_v_data_t* d)
{
	if(d->ts_monotonic_us<0) return 0;
	if(d->time_utc_us<0) return 0;
	if(isnan(d->vn)) return 0;
	if(isnan(d->ve)) return 0;
	if(isnan(d->vd)) return 0;
	if(isnan(d->v_variance)) return 0;
	if(d->fix_type<0) return 0;

	return 1;
}


static void _slpi_add_gps_data(gps_v_data_t d)
{
	static int has_sent_first = 0;

	// no need to do anything unless we have imu/camera data
	if(!is_imu_connected) return;
	if(!is_cam_connected) return;
	if(!is_initialized) return;

	// if no fix, don't continue
	if(d.fix_type<=0){
		if(en_debug_gps){
			printf("DEBUG GPS no fix\n");
		}
		return;
	}

	if(en_debug_gps){
		printf("new gps data: Variance: %4.1f vNED: %5.2f %5.2f %5.2f\n",\
				(double) d.v_variance, (double)d.vn, (double)d.ve, (double)d.vd);
	}

	// skip terrible gps
	float max_variance = 1.5f;
	if(d.v_variance > max_variance){
		if(en_debug_gps){
			printf("DEBUG GPS velocity variance too high: %0.2f>%0.2f\n", (double)d.v_variance, (double)max_variance);
		}
		return;
	}

	// only populate the diagonals for covarience matrix
	// treat speed accuracy as 1 standard deviation, square to get variance
	float64_t measCovVelocity[3][3] = {{0,0,0},{0,0,0},{0,0,0}};
	measCovVelocity[0][0] = d.v_variance;
	measCovVelocity[1][1] = d.v_variance;
	measCovVelocity[2][2] = d.v_variance;

	int64_t sys_time_ns = d.ts_monotonic_us * 1000;
	int64_t gps_time_ns = d.time_utc_us * 1000;

	if(!has_sent_first){
		printf("sending in first GPS packet\n");
		has_sent_first = 1;
	}

	// send data into vio!
	if(!is_initialized) return;
	pthread_mutex_lock(&mv_mtx);
	mvVISLAM_AddGPStimeSync(mv_vislam_ptr,
			sys_time_ns,
			gps_time_ns - sys_time_ns,
			-1);
	mvVISLAM_AddGPSvelocity(mv_vislam_ptr,
			gps_time_ns,
			d.ve, d.vn, -d.vd,
			measCovVelocity, 4);
	pthread_mutex_unlock(&mv_mtx);


	return;
}


// return 0 on successful packet parse
// return -1 on some parsing error, no data
// return -2 if the topic has not been published yet, indicating gps driver
// is still starting or is not connected to a gps module.
static int _reader(FILE* fp, gps_v_data_t* d)
{
	char buf[PX4_LISTENER_BUFSIZE];

	_reset_gps_v_data(d);

	while(fgets(buf, PX4_LISTENER_BUFSIZE, fp) != NULL) {

		//printf("parsing: %s", buf);

		if(_starts_with(buf, " Topic did not match any known topics")){
			printf("GPS DEBUG: %s", buf);
			return -1;
		}
		else if(_starts_with(buf, "never published")){
			printf("GPS DEBUG: %s", buf);
			return -2;
		}
		else if(_starts_with(buf, "TOPIC: vehicle_gps_position")){
			// beginning of packet, make sure struct is reset
			_reset_gps_v_data(d);
			continue;
		}
		else if(_starts_with(buf, "\ttimestamp:")){
			sscanf(buf, "\ttimestamp:%lld", &d->ts_monotonic_us);
		}
		else if(_starts_with(buf, "\ttime_utc_usec:")){
			sscanf(buf, "\ttime_utc_usec:%lld", &d->time_utc_us);
		}
		else if(_starts_with(buf, "\ts_variance_m_s:")){
			sscanf(buf, "\ts_variance_m_s:%f", &d->v_variance);
		}
		else if(_starts_with(buf, "\tvel_n_m_s:")){
			sscanf(buf, "\tvel_n_m_s:%f", &d->vn);
		}
		else if(_starts_with(buf, "\tvel_e_m_s:")){
			sscanf(buf, "\tvel_e_m_s:%f", &d->ve);
		}
		else if(_starts_with(buf, "\tvel_d_m_s:")){
			sscanf(buf, "\tvel_d_m_s:%f", &d->vd);
		}
		else if(_starts_with(buf, "\tfix_type:")){
			sscanf(buf, "\tfix_type:%d", &d->fix_type);
			if(_is_gps_v_data_complete(d)){
				return 0;
			}
		}
	}

	return -1;
}


static void* _slpi_gps_reader_function(__attribute__((unused)) void* arg)
{
	FILE *fp;
	int ret;
	gps_v_data_t gps_v_data;
	int64_t last_ts = 0;
	_reset_gps_v_data(&gps_v_data);

	printf("starting slpi gps thread\n");

	// first wait for px4 daemon to start
	while(main_running){

		// try once a second
		sleep(1);

		// make sure voxl-px4 is running
		ret = system("systemctl status voxl-px4 | grep -q -e \"active (running)\" ");
		if(ret==0){
			printf("detected voxl-px4 has started\n");
			break;
		}
		else if(en_debug_gps) printf("DEBUG GPS: voxl-px4 has not started yet\n");
	}

	// give px4 10 seconds to finish starting
	if(en_debug_gps) printf("DEBUG GPS: waiting for voxl-px4 to finish starting\n");
	sleep(10);


	// now keep trying px4-listener
	while(main_running){

		// gps updates at 10hz, read at 20 so we don't miss any
		// duplicates are thrown away later
		usleep(50000);

		if((fp = popen("px4-listener vehicle_gps_position -n 1", "r")) == NULL) {
			perror("Error opening pipe from px4 listener");
			continue;
		}

		ret = _reader(fp, &gps_v_data);
		if(ret==0){
			// only act on new packets, there will be lots of duplicates
			if(gps_v_data.ts_monotonic_us != last_ts){
				last_ts = gps_v_data.ts_monotonic_us;
				_slpi_add_gps_data(gps_v_data);
			}
		}
		else if(ret==-2 && main_running){
			if(en_debug_gps){
				printf("DEBUG GPS extra sleep waiting for GPS first publication\n");
			}
			sleep(1);
		}

		pclose(fp);

	}

	return NULL;
}

static int _slpi_gps_reader_init(void)
{
	pthread_create(&slpi_gps_thread, NULL, _slpi_gps_reader_function, NULL);
	return 0;
}

static int _slpi_gps_reader_stop(void)
{
	pthread_join(slpi_gps_thread, NULL);
	return 0;
}

